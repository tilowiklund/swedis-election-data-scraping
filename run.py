from bs4 import BeautifulSoup
import codecs
import os
import urllib.request
import urllib.parse
import shutil
import re

from random import randint
from time import sleep

if not os.path.exists('./url'):
    with open('./url', 'w') as f:
        f.write("https://data.val.se/val/val2018/slutresultat/R/rike/index.html")

with open('./url', 'r') as f:
    base_url = f.read()

if not os.path.exists('./index.html'):
    with open('./url', 'r') as f:
        urllib.request.urlretrieve(f.read(), './index.html')

if not os.path.exists('./results'):
    os.makedirs('./results')

with codecs.open('./index.html', 'r', 'iso-8859-1') as f:
    bs = BeautifulSoup(f.read(), 'html.parser')

    table = bs.find('table', { 'summary' : "Den här tabellen visar antal och andel röster per parti i Sverige." })
    head = table.find('thead').find('tr').find_all('th', { 'class' : "relativ_nej" })
    # for h in head:
    #     print(h.decode_contents())
    # print("+++++++++++++++++++++++++++")
    for row in table.find('tbody').find_all(lambda tag: tag.name == 'tr' and (tag.get('class') is None or not ('summa' in tag.get('class')))):
        row_name = row.find('td').find('a')
        print(row_name.decode_contents())
        # for cell in row.find_all('td', { 'class' : "relativ_nej" }):
        #     print(cell.decode_contents())
        #     print("---------------------------")
        row_path = os.path.join('./results', row_name.get('title'))
        county_url = urllib.parse.urljoin(base_url, row_name.get('href'))
        county_index_path = os.path.join(row_path, 'index.html')
        if not os.path.exists(row_path):
            os.makedirs(row_path)
            with open(os.path.join(row_path, 'url'), 'w') as f:
                f.write(county_url)
        if not os.path.exists(county_index_path):
            urllib.request.urlretrieve(county_url, county_index_path)

        with codecs.open(county_index_path, 'r', 'iso-8859-1') as g:
            county_bs = BeautifulSoup(g.read(), 'html.parser')

            # county_table_summary = "Den här tabellen visar antal och andel röster per parti i {}.".format(row_name.get('title'))
            # county_table = county_bs.find('table', { 'summary' : county_table_summary })
            county_table_summary_prefix = "Den här tabellen visar antal och andel röster per parti i"
            county_tables = county_bs.find_all(lambda tab: tab.name == 'table' and (not tab.get('summary') is None) and tab.get('summary').startswith(county_table_summary_prefix))

            if len(county_tables) != 1:
                print("???????????????????????????????????")
                exit(1)
            county_table = county_tables[0]

            county_head = county_table.find('thead').find('tr').find_all('th', { 'class' : "relativ_nej" })
            # for h in county_head:
            #     print(h.decode_contents())
            # print("+++++++++++++++++++++++++++")

            region_district_markers = [ h for h in county_bs.find_all('h2') if re.match('Röstfördelning per ((valdistrikt)|(kommun))', h.decode_contents()) ]
            if(len(region_district_markers) != 1):
                print("Unclear whether this is a region or district")
                exit(1)
            is_region = not re.match('Röstfördelning per kommun', region_district_markers[0].decode_contents()) is None
            print(is_region)

            if is_region:
              for county_row in county_table.find('tbody').find_all(lambda tag: tag.name == 'tr' and (tag.get('class') is None or not ('summa' in tag.get('class')))):
                  county_row_name = county_row.find('td').find('a')

                  print('\t' + county_row_name.decode_contents())

                  county_row_path = os.path.join(os.path.join(row_path, 'results'), county_row_name.get('title'))
                  if not os.path.exists(os.path.join(row_path, 'results')):
                      os.makedirs(os.path.join(row_path, 'results'))

                  municipality_url = urllib.parse.urljoin(county_url, county_row_name.get('href'))
                  municipality_index_path = os.path.join(county_row_path, 'index.html')

                  if not os.path.exists(county_row_path):
                      os.makedirs(county_row_path)
                      with open(os.path.join(county_row_path, 'url'), 'w') as h:
                          h.write(municipality_url)

                  if not os.path.exists(municipality_index_path):
                      urllib.request.urlretrieve(municipality_url, municipality_index_path)
                      sleep(randint(0,3))

                  # for cell in county_row.find_all('td', { 'class' : "relativ_nej" }):
                  #     print(cell.decode_contents())
                  #     print("---------------------------")

              ### This should REALLY be extracted into a function we can reuse three times :P
