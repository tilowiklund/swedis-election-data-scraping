from bs4 import BeautifulSoup
import codecs
import pandas as pd
import os
import re

total = None
for r,ds,fs in os.walk("./results"):
    if(len(ds) == 0 and 'index.html' in fs):
        m1 = re.match("^\./results/([^/]+)/results/([^/]+)$", r)
        if not m1 is None:
            region,munic = m1.groups()
        else:
            m2 = re.match("^\./results/([^/]+)$", r)
            if not munic is None:
                region = m2.groups()[0]
                munic = region
            else:
                print("Unable to parse {}".format(r))
                exit(1)

        print("{} ---> {}/{}".format(r, region, munic))

        with codecs.open(os.path.join(r, 'index.html'), 'r', 'iso-8859-1') as f:
            bs = BeautifulSoup(f.read(), 'html.parser')

        table_summary_prefix = "Den här tabellen visar antal och andel röster per parti i"
        tables = [tab for tab in bs.find_all('table') if (not tab.get('summary') is None) and tab.get('summary').startswith(table_summary_prefix) ]
        if len(tables) != 1:
            print("???????????????????????????????????")
            exit(1)
        table = tables[0]

        def int_or(x, default=0):
            try:
                return int(x)
            except ValueError:
                return default

        head = [h.decode_contents() for h in table.find('thead').find('tr').find_all('th', { 'class' : "relativ_nej" })]

        forbid = ['summa', 'delsumma']

        def parse_href(href):
            try:
                return re.match("\.\./\.\./\.\./\.\./(?:\.\./){0,2}slutresultat/R/((?:valdistrikt)|(?:onsdagsdistrikt))/([0-9]{2})/([0-9]{2})/([0-9]{2,4})/index.html", href.get('href')).groups() + (href.decode_contents(),)
            except:
                print(":(")
                print(href)
                import pdb; pdb.set_trace()

        body = { parse_href(row.find('td').find('a')) : [ int_or(cell.decode_contents()) for cell in row.find_all('td', { 'class' : "relativ_nej" })  ]
                 for row in table.find('tbody').find_all('tr')
                 if row.get('class') is None
                   or not any([f in row.get('class') for f in forbid ]) }

        final = pd.DataFrame(body, index=head).T

        if not os.path.exists(os.path.join(r, "votes.csv")):
            final.to_csv(os.path.join(r, "votes.csv"), index_label="district")

        final['region'] = region
        final['municipality'] = munic

        if total is None:
            total = final
        else:
            total = total.append(final)

total.to_csv("all_votes.csv", index_label="district")
